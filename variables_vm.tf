/*
* Unpublished work.
* Copyright 2021-2023 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
# Terraform variables used in main script
#


#
# VM specific variables
#

variable "vm_ami_name" {
  # Put here a custom Vionlabs AMI
  description = "AMI (Amazon Machine Image) to create VM from"
  type        = string
}

variable "vm_instance_type" {
  description = "VM instance type (eg: t3.2xlarge, etc). Ref to https://aws.amazon.com/ec2/instance-types/ for full list"
  type        = string
  default     = "t3a.2xlarge"  # 8 CPU, 32G RAM
}

variable "vm_instance_count" {
  description = "VM instance count to create"
  type        = number
  default     = 1
}

variable "vm_do_monitoring" {
  description = "Monitoring support for deployed VM (charts for CPU/Disk/RAM on AWS dashboard for VM)"
  type        = bool
  default     = false
}

variable "vm_iam_profile" {
  # For details ref to https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_switch-role-ec
  description = "IAM Instance Profile to launch the VM instance with"
  type        = string
  default     = "Vionlabs_vm_iam_profile"
}

variable "vm_root_volume_size_gb" {
  description = "VM instance: size of the volume in gibibytes (GiB)"
  type        = number
  default     = 100
}

variable "vm_root_volume_type" {
  description = "VM instance: type of volume (eg: gp2, gp3, etc). Ref to https://aws.amazon.com/ebs/volume-types/ for details"
  type        = string
  default     = "gp3"
}

variable "vm_subnet_id" {
  description = "VM instance: subnet that VM will be running in"
  type = string
}

variable "vm_net_cidr" {
  type = string
  description = "VM Networking setup: CIDR range"
  default = "10.13.0.0/24"
}

variable "vm_net_availability_zone" {
  type = string
  description = "VM Networking setup: Availability Zone of subnet"
}

variable "vm_ssh_dev" {
  description = "Enable/disable SSH for deployed VM"
  type = bool
  default = false
}

variable "vm_script" {
  description = "Part of Cloud Init setup script for VM"
  type = string
  default = ""
}
