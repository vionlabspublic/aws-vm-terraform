/*
* Unpublished work.
* Copyright 2021-2023 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
#
# Role for VM to operate (vm_instance_type variable)
# requires the following permissions:
#
# - s3 input bucket read (usually different AWS account)
# - s3 output bucket write
# - write to events sns
# - read from batches sqs
#




#
#
# AWS EC2 VM Instance(s) deployment
#
#


#
# !!! NOTE: security group is a temp facility to access created VM(s) by SSH
#
# to disable it:
# comment out aws_security_group and 
# vpc_security_group_ids attribute inside aws_instance resource
#


# Define a security group that will allow connection through port 22.
# It will also forward all traffic without restriction.
# The ingress block is used to describe how incoming traffic will be treated.
# Here we have defined a rule to accept connections from all IPs on port 22.
# The egress block defines the rule for outgoing traffic
#
resource "aws_security_group" "ingress-all" {
  name = "vionlabs-sg"
  ingress {
    description = "SSH"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }

  # Terraform removes the default rule, recreate
  # Allow all outbound traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.My_VPC.id

  tags = var.tags
}

resource "aws_security_group" "ingress-none" {
  name = "vionlabs-sg-none"

  # Enable https (port 443)
  # to talk with AWS s3/sqs/sns we need https egress

  # Terraform removes the default rule, recreate
  # Allow only secure outbound traffic
  egress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.My_VPC.id

  tags = var.tags
}



#
# Instantiate VM inside AWS EC2
#

resource "aws_instance" "vm_vionlabs" {

  count = var.vm_instance_count

  ami = var.vm_ami_name

  instance_type = var.vm_instance_type

  monitoring = var.vm_do_monitoring

  iam_instance_profile = var.vm_iam_profile

  root_block_device  {
    volume_size = var.vm_root_volume_size_gb
    volume_type = var.vm_root_volume_type
  }

  # Instead of existing subnet - use created one
  #subnet_id = var.vm_subnet_id
  subnet_id = aws_subnet.My_VPC_Subnet_Private.id

  # comment/remove below attribute out to disable SSH access
  vpc_security_group_ids = [var.vm_ssh_dev ? aws_security_group.ingress-all.id : aws_security_group.ingress-none.id]

  # Pass some user data into VM
  user_data = var.vm_user_data

  # enable IPv4 only if SSH is on
  # associate_public_ip_address = var.vm_ssh_dev

  metadata_options {
    # Require usage of the newer v2 metadata API:
    #http_endpoint = "enabled"
    #http_tokens = "required"  # MDSv2
    http_put_response_hop_limit = 2 # https://github.com/argoproj/argo-workflows/issues/9811
  }

  tags = var.tags
}
