/*
* Unpublished work.
* Copyright 2021-2024 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/

#
# Create secured network env - VPC/priv subnet/VPC endpoints, etc
#

locals {
  vpcCIDRblock    = var.vm_net_cidr
  subnetCIDRblock = var.vm_net_cidr
}

# create the VPC
resource "aws_vpc" "My_VPC" {
  cidr_block           = local.vpcCIDRblock

  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = var.tags
}

# create the Subnet
resource "aws_subnet" "My_VPC_Subnet_Private" {
  vpc_id                  = aws_vpc.My_VPC.id
  cidr_block              = local.subnetCIDRblock
  #map_public_ip_on_launch = true

  availability_zone       = var.vm_net_availability_zone

  tags = var.tags
}

/* Not used in private mode
# create the Internet Gateway
resource "aws_internet_gateway" "My_VPC_GW" {
  vpc_id = aws_vpc.My_VPC.id

  tags = var.tags
}
*/

# modify main route table for the subnet: add 0.0.0.0/0 via Internet Gateway
resource "aws_route_table" "My_VPC_PRIVATE_route_table" {
  vpc_id = aws_vpc.My_VPC.id

  /* Use when IGW enabled
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.My_VPC_GW.id
  }
  */
  tags = var.tags
}

resource "aws_route_table_association" "My_VPC_Private_association" {
  subnet_id      = aws_subnet.My_VPC_Subnet_Private.id
  route_table_id = aws_route_table.My_VPC_PRIVATE_route_table.id
}

# create the dedicated Security Group
resource "aws_security_group" "My_VPC_Security_Group_Private" {
  vpc_id      = aws_vpc.My_VPC.id
  name        = "Vionlabs VPC Security Group Private"

  ingress {
    description = "https"
    cidr_blocks = [local.subnetCIDRblock]
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
  }
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = var.tags
}


#
# PrivateLink (VPC Endpoints) setup
#

#
# S3 VPCE
#

data "aws_vpc_endpoint_service" "s3" {
  service      = "s3"
  service_type = "Gateway"
}

resource "aws_vpc_endpoint" "s3" {
  vpc_id       = aws_vpc.My_VPC.id
  service_name = data.aws_vpc_endpoint_service.s3.service_name
  vpc_endpoint_type = "Gateway"

  # N/A until input bucket will be refered in tf...
  # policy = var.combined_s3_policy

  tags = var.tags
}

# associate route table with s3 VPC endpoint
resource "aws_vpc_endpoint_route_table_association" "Private_route_table_association_s3_vpce" {
  route_table_id  = aws_route_table.My_VPC_PRIVATE_route_table.id
  vpc_endpoint_id = aws_vpc_endpoint.s3.id
}

#
# SQS VPCE
#

data "aws_vpc_endpoint_service" "sqs" {
  service      = "sqs"
  service_type = "Interface"
}

resource "aws_vpc_endpoint" "sqs" {
  vpc_id       = aws_vpc.My_VPC.id
  service_name = data.aws_vpc_endpoint_service.sqs.service_name
  vpc_endpoint_type = "Interface"
  private_dns_enabled = true

  subnet_ids         = [aws_subnet.My_VPC_Subnet_Private.id]
  security_group_ids = [aws_security_group.My_VPC_Security_Group_Private.id]

  policy = var.combined_sqs_policy

  tags = var.tags
}

#
# SNS VPCE
#

data "aws_vpc_endpoint_service" "sns" {
  service      = "sns"
  service_type = "Interface"
}
resource "aws_vpc_endpoint" "sns" {
  vpc_id       = aws_vpc.My_VPC.id
  service_name = data.aws_vpc_endpoint_service.sns.service_name
  vpc_endpoint_type = "Interface"
  private_dns_enabled = true

  subnet_ids         = [aws_subnet.My_VPC_Subnet_Private.id]
  security_group_ids = [aws_security_group.My_VPC_Security_Group_Private.id]

  policy = var.combined_sns_policy

  tags = var.tags
}
