/*
* Unpublished work.
* Copyright 2021-2023 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
# Edge Customer's infrastructure creation
#
# Terraform needs the following rights:
#
# 1) for infra bring-up:
# s3  create
# sqs create
# sns create
#
#
# 2) role for VM to operate:
# s3 input bucket read
# s3 output bucket write
# write to events sns
# read from batches sqs
#
#



# Environment check to ensure correct matching of workspace and vars
locals {
#  environment_check = index(terraform.workspace != var.env ? []:[terraform.workspace], terraform.workspace)
}


locals {

  res_labels = var.tags 
}



#
# Module which creates infrastructure for Edge Processing (c) Vionlabs
#

module "remote-infra" {
  source = "./remote-infra"

  customer_prefix = var.customer_prefix

  input_bucket  = var.input_bucket
  output_bucket = var.output_bucket
  secure_bucket = var.secure_bucket

  batches_sns   = var.batches_sns
  batches_sqs   = var.batches_sqs

  events_sns    = var.events_sns
  events_sqs    = var.events_sqs

  tags          = var.tags
}

#
# Write VM configuration script into file
# to be consumed by VM
#

resource "local_file" "vm_script" {
  content    = local.vm_script
  filename   = "vm-startup-script.sh"

  depends_on = [module.remote-infra]
}
