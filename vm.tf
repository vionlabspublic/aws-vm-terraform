/*
* Unpublished work.
* Copyright 2021-2024 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
#
# Role for VM to operate (vm_instance_type variable)
# requires the following permissions:
#
# - s3 input bucket read (usually different AWS account)
# - s3 output bucket write
# - write to events sns
# - read from batches sqs
#



# Environment check to ensure correct matching of workspace and vars
locals {
#  environment_check = index(terraform.workspace != var.env ? []:[terraform.workspace], terraform.workspace)
}


#
# Instantiate VM inside AWS EC2
#

module "aws_vm_vionlabs" {
  source = "./aws-vm"

  vm_instance_count = var.vm_instance_count
  vm_ami_name       = var.vm_ami_name
  vm_instance_type  = var.vm_instance_type
  vm_do_monitoring  = var.vm_do_monitoring
  vm_iam_profile    = module.remote-infra.vm_iam_profile # var.vm_iam_profile
  vm_subnet_id      = var.vm_subnet_id
  vm_ssh_dev        = var.vm_ssh_dev
  vm_net_cidr       = var.vm_net_cidr
  vm_net_availability_zone = var.vm_net_availability_zone

  vm_root_volume_size_gb = var.vm_root_volume_size_gb
  vm_root_volume_type    = var.vm_root_volume_type

  # Pass some user data into VM (configure parameters)
  vm_user_data = local.vm_script

  combined_s3_policy  = module.remote-infra.combined_s3_policy
  combined_sqs_policy = module.remote-infra.combined_sqs_policy
  combined_sns_policy = module.remote-infra.combined_sns_policy

  tags = var.tags
}
