/*
* Unpublished work.
* Copyright 2021-2024 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
#
# Terraform module for backend system user/policies
#
# Access rights needed:
#
# s3 output bucket read
# read from events sqs
# write to batches sns
# purge batches sqs
#





# Environment check to ensure correct matching of workspace and vars
locals {
  #  environment_check = index(terraform.workspace != var.env ? []:[terraform.workspace], terraform.workspace)
}



#
# AWS User for Vionlabs Backend System
#

resource "aws_iam_user" "vionlabs" {
  name = "${var.customer_prefix}-backend-aws"

  tags = var.tags
}

resource "aws_iam_access_key" "vionlabs" {
  user = aws_iam_user.vionlabs.name
}


#
# Policy doc to allow reading objects from output s3 bucket
#

data "aws_iam_policy_document" "output-bucket-reader" {
  statement {
    actions = [
      "s3:ListBucket",
      "s3:ListBucketVersions",
    ]

    resources = [
      aws_s3_bucket.output_bucket.arn
    ]

    effect = "Allow"
  }

  statement {
    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion"
    ]

    resources = [
      "${aws_s3_bucket.output_bucket.arn}/*"
    ]

    effect = "Allow"
  }
}

#
# Policy doc to allow reading messages from events SNS topic
#

data "aws_iam_policy_document" "events-reader" {
  statement {

    sid = "eventsreader"

    actions = [
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage"
    ]

    resources = [
      aws_sqs_queue.sqs_events.arn
    ]

    effect = "Allow"
  }
}

#
# Policy doc to allow writing messages to batches SQS queue
#

data "aws_iam_policy_document" "batches-writer" {
  statement {

    sid = "batcheswriter"

    actions = [
      "sns:Publish"
    ]

    resources = [
      aws_sns_topic.sns_batches.arn
    ]

    effect = "Allow"
  }
}

#
# Policy doc to allow purging baches SQS queue
#

data "aws_iam_policy_document" "batches-purger" {
  statement {

    sid = "batchespurger"

    actions = [
      "sqs:PurgeQueue"
    ]

    resources = [
      aws_sqs_queue.sqs_batches.arn
    ]

    effect = "Allow"
  }
}

#
# Attach policies to the backend IAM user
#

resource "aws_iam_user_policy" "output-bucket-reader" {
  name = "${var.customer_prefix}-policy-output-bucket-reader"
  user = aws_iam_user.vionlabs.name

  policy = data.aws_iam_policy_document.output-bucket-reader.json
}
# writes to output bucket some artifacts for some FE
resource "aws_iam_user_policy" "output-bucket-writer" {
  name = "${var.customer_prefix}-policy-output-bucket-writer"
  user = aws_iam_user.vionlabs.name

  policy = data.aws_iam_policy_document.output-bucket-writer.json
}


resource "aws_iam_user_policy" "events-reader" {
  name = "${var.customer_prefix}-policy-events-reader"
  user = aws_iam_user.vionlabs.name

  policy = data.aws_iam_policy_document.events-reader.json
}
resource "aws_iam_user_policy" "batches-writer" {
  name = "${var.customer_prefix}-policy-batches-writer"
  user = aws_iam_user.vionlabs.name

  policy = data.aws_iam_policy_document.batches-writer.json
}
resource "aws_iam_user_policy" "batches-purger" {
  name = "${var.customer_prefix}-policy-batches-purger"
  user = aws_iam_user.vionlabs.name

  policy = data.aws_iam_policy_document.batches-purger.json
}
