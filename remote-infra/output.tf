/*
* Unpublished work.
* Copyright 2021-2024 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
# Print out provisioned resources by TF
#

output "s3_input_bucket" {
  value       = var.input_bucket
}

output "s3_output_bucket_arn" {
  value       = aws_s3_bucket.output_bucket.arn
}

output "s3_secure_bucket_arn" {
  value       = aws_s3_bucket.secure_bucket.arn
}

output "sns_batches_arn" {
  value       = aws_sns_topic.sns_batches.arn
}

output "sqs_batches_url" {
  value       = aws_sqs_queue.sqs_batches.url
}

output "sns_events_arn" {
  value       = aws_sns_topic.sns_events.arn
}

output "sqs_events_url" {
  value       = aws_sqs_queue.sqs_events.url
}

output "backend-aws-id" {
  value = aws_iam_access_key.vionlabs.id
}

output "backend-aws-secret" {
  value = aws_iam_access_key.vionlabs.secret
}

output "vm_iam_profile" {
  value = aws_iam_instance_profile.vm.name
}

output "combined_s3_policy" {
  value = data.aws_iam_policy_document.combined_s3_policy.json
}

output "combined_sqs_policy" {
  value = data.aws_iam_policy_document.combined_sqs_policy.json
}

output "combined_sns_policy" {
  value = data.aws_iam_policy_document.combined_sns_policy.json
}
