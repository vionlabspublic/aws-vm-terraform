/*
* Unpublished work.
* Copyright 2021-2024 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
# Terraform variables used in main script
#

variable "tags" {
  description = "Resource labels. Used to tag any compatible cloud object with user data"
  default = {}
}

variable "customer_prefix" {
    type = string
    description = "Prefix to prepend before customer's resource names"
}

#
# Infrastructure specific variables
#
# Ref to docs regarding attributes and usage
#
# AWS S3
# https://docs.aws.amazon.com/s3/
#
# AWS SNS
# https://docs.aws.amazon.com/sns/
#
# AWS SQS
# https://docs.aws.amazon.com/sqs/
#
# AWS EC2
# https://docs.aws.amazon.com/ec2/
#

variable "input_bucket" {
  description = "Infrastructure: AWS S3 input bucket name (holds customer assets)"
  type = string
}

variable "output_bucket" {
  description = "Infrastructure: AWS S3 output bucket name (holds generated artifacts)"
  type = string
}

variable "secure_bucket" {
  description = "Infrastructure: AWS S3 secure bucket name (holds generated secured artifacts)"
  type = string
}

variable "batches_sns" {
  description = "Infrastructure: AWS batches SNS name"
  type = string
}

variable "batches_sqs" {
  description = "Infrastructure: AWS batches SQS name"
  type = string
}

variable "events_sns" {
  description = "Infrastructure: AWS events SNS name"
  type = string
}

variable "events_sqs" {
  description = "Infrastructure: AWS events SQS name"
  type = string
}
