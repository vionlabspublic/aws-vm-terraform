/*
* Unpublished work.
* Copyright 2021-2024 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
#
# Infrastructure module needs the following rights:
#
# - s3 bucket create
# - sqs create
# - sns create
# - IAM create policies/roles
#


#
# S3 buckets provisioning
#


# Note: it is assumed that S3 input bucket is already created
# and just communicated to the system as provisioning parameter
# For dev/testing purpose it's being created here

#
# Input bucket with customer media assets to be read & processed
#
/* Never create input bucket, but read its attributes (if provided)
resource "aws_s3_bucket" "input_bucket" {
  bucket = var.input_bucket
  acl    = "private"

  versioning {
    enabled = true
  }

  tags = merge({
    hint        = "S3 bucket for keeping input objects"
  }, var.tags)
}

#
# Make is as much private as possible
#
resource "aws_s3_bucket_public_access_block" "input_bucket_public_off" {
  bucket = aws_s3_bucket.input_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
*/

# Fetch S3 bucket details if input_bucket is provided
data "aws_s3_bucket" "input_bucket" {
  count  = var.input_bucket != "" ? 1 : 0

  bucket = var.input_bucket
}

#
# Output bucket with generated artifacts
# Note: objects in bucket expire in 30 days
#

resource "aws_s3_bucket" "output_bucket" {
  bucket = var.output_bucket

  tags = merge({
    hint        = "S3 bucket for keeping output artifacts"
  }, var.tags)
}
resource "aws_s3_bucket_lifecycle_configuration" "output_bucket_lifecycle" {
  bucket = aws_s3_bucket.output_bucket.id

  rule {
    id     = "s3_exp_1_month"

    expiration {
      days = 30
    }

    status = "Enabled"
  }
}
resource "aws_s3_bucket_versioning" "output_bucket_versioning" {
  bucket = aws_s3_bucket.output_bucket.id

  versioning_configuration {
    status = "Enabled"
  }
}


#
# Make is as much private as possible
#
resource "aws_s3_bucket_public_access_block" "output_bucket_public_off" {
  bucket = aws_s3_bucket.output_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

#
# Secure bucket with generated artifacts
# Note: objects in bucket being kept private in customer infra only
# Only VM writes into it, no read policies defined

resource "aws_s3_bucket" "secure_bucket" {
  bucket = var.secure_bucket

  tags = merge({
    hint        = "S3 bucket for keeping secured artifacts"
  }, var.tags)
}
resource "aws_s3_bucket_versioning" "secure_bucket_versioning" {
  bucket = aws_s3_bucket.secure_bucket.id

  versioning_configuration {
    status = "Enabled"
  }
}


#
# Make is as much private as possible
#
resource "aws_s3_bucket_public_access_block" "secure_bucket_public_off" {
  bucket = aws_s3_bucket.secure_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}


#
# SNS + SQS provisioning
#

#
# Forward path (Vionlabs -> Customer AWS)
#

resource "aws_sns_topic" "sns_batches" {
  name = var.batches_sns

  tags = var.tags
}

resource "aws_sqs_queue" "sqs_batches" {
  name = var.batches_sqs

  tags = var.tags
}

#
# Create subscription between sns - sqs
#
resource "aws_sns_topic_subscription" "subs_batches" {
  topic_arn = aws_sns_topic.sns_batches.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.sqs_batches.arn
}

#
# Backward path (Customer AWS -> Vionlabs)
# Note: used to report status, progress, events 
#

resource "aws_sns_topic" "sns_events" {
  name = var.events_sns

  tags = var.tags
}

resource "aws_sqs_queue" "sqs_events" {
  name = var.events_sqs

  tags = var.tags
}

#
# Create subscription between sns - sqs services
#
resource "aws_sns_topic_subscription" "subs_events" {
  topic_arn = aws_sns_topic.sns_events.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.sqs_events.arn
}

#
# Glue policy to allow sending messages to fwd queue
#

#Note: Sid = Statement ID
resource "aws_sqs_queue_policy" "sqs_batches_policy" {
    queue_url = aws_sqs_queue.sqs_batches.id

    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "sqspolicy",
  "Statement": [
    {
      "Sid": "First",
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "sqs:SendMessage",
      "Resource": "${aws_sqs_queue.sqs_batches.arn}",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "${aws_sns_topic.sns_batches.arn}"
        }
      }
    }
  ]
}
POLICY
}

#
# Glue policy to allow sending messages to bkw queue
#

resource "aws_sqs_queue_policy" "sqs_events_policy" {
    queue_url = aws_sqs_queue.sqs_events.id

    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "sqspolicy",
  "Statement": [
    {
      "Sid": "First",
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "sqs:SendMessage",
      "Resource": "${aws_sqs_queue.sqs_events.arn}",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "${aws_sns_topic.sns_events.arn}"
        }
      }
    }
  ]
}
POLICY
}

data "aws_caller_identity" "current" {
}


# Note: might be a manual operation described in readme
#
# Policy doc to allow reading objects from input s3 bucket
#

data "aws_iam_policy_document" "input-bucket-reader" {

    count = var.input_bucket != "" ? 1 : 0

    statement {
        actions = [
            "s3:ListBucket",
            "s3:ListBucketVersions",
        ]

        resources = [
            data.aws_s3_bucket.input_bucket[0].arn
        ]

        effect = "Allow"
    }

    statement {
        actions = [
            "s3:GetObject"
        ]

        resources = [
            "${data.aws_s3_bucket.input_bucket[0].arn}/*"
        ]

        effect = "Allow"
    }
}

#
# Policy doc to allow writing objects to output s3 bucket
#

data "aws_iam_policy_document" "output-bucket-writer" {
    statement {
        actions = [
            "s3:ListBucket",
            "s3:ListBucketVersions",
        ]

        resources = [
            aws_s3_bucket.output_bucket.arn
        ]

        effect = "Allow"
    }

    statement {
        actions = [
            "s3:PutObject"
        ]

        resources = [
            "${aws_s3_bucket.output_bucket.arn}/*"
        ]

        effect = "Allow"
    }
}

#
# Policy doc to allow writing objects to secure s3 bucket
#

data "aws_iam_policy_document" "secure-bucket-writer" {
    statement {
        actions = [
            "s3:ListBucket",
            "s3:ListBucketVersions",
        ]

        resources = [
            aws_s3_bucket.secure_bucket.arn
        ]

        effect = "Allow"
    }

    statement {
        actions = [
            "s3:PutObject"
        ]

        resources = [
            "${aws_s3_bucket.secure_bucket.arn}/*"
        ]

        effect = "Allow"
    }
}

#
# Policy doc to allow reading messages from batches queue
#

data "aws_iam_policy_document" "batches-reader" {
    statement {

        sid = "batchesreader"

        actions = [
            "sqs:ReceiveMessage",
            "sqs:DeleteMessage"
        ]

        resources = [
            aws_sqs_queue.sqs_batches.arn
        ]

        effect = "Allow"
    }
}

#
# Policy doc to allow writing messages to events SNS topic
#

data "aws_iam_policy_document" "events-writer" {
    statement {

        sid = "eventswriter"

        actions = [
            "sns:Publish"
        ]

        resources = [
            aws_sns_topic.sns_events.arn
        ]

        effect = "Allow"
    }
}

#
# Policy document for sts assume role trust relationship
#
data "aws_iam_policy_document" "assume-role" {
    statement {
        actions = ["sts:AssumeRole"]

        principals {
          type        = "Service"
          identifiers = ["ec2.amazonaws.com"]
        }

        effect = "Allow"
    }
}



#
# Policies for VPC endpoints
# those are same as for VM role and backend user
# but need Principals specified
#


# Combine the policies for S3 into one document
# With gateway endpoints, the Principal element must be set to *. To specify a principal, use the aws:PrincipalArn condition key
# https://docs.aws.amazon.com/vpc/latest/privatelink/vpc-endpoints-access.html
data "aws_iam_policy_document" "combined_s3_policy" {
  statement {
    actions = [
        "s3:ListBucket",
        "s3:ListBucketVersions",
    ]
    resources = [
        aws_s3_bucket.output_bucket.arn,
        aws_s3_bucket.secure_bucket.arn
    ]
    principals {
      type = "*"
      identifiers = ["*"]
    }
    condition {
      test     = "StringEquals"
      variable = "aws:PrincipalArn"
      values   = [aws_iam_role.Vionlabs_ec2_vm_role.arn]
    }
    effect = "Allow"
  }
  statement {
    actions = [
      "s3:PutObject"
    ]
    resources = [
      "${aws_s3_bucket.output_bucket.arn}/*",
      "${aws_s3_bucket.secure_bucket.arn}/*"
    ]
    principals {
      type = "*"
      identifiers = ["*"]
    }
    condition {
      test     = "StringEquals"
      variable = "aws:PrincipalArn"
      values   = [aws_iam_role.Vionlabs_ec2_vm_role.arn]
    }
    effect = "Allow"
  }
}

# Combine the policies for SQS into one document
data "aws_iam_policy_document" "combined_sqs_policy" {

  statement {

    sid = "batchesreader"

    actions = [
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage"
    ]

    resources = [
      aws_sqs_queue.sqs_batches.arn
    ]

    principals {
      type = "AWS"
      identifiers = [aws_iam_role.Vionlabs_ec2_vm_role.arn]
    }

    effect = "Allow"
  }

  statement {

    sid = "eventsreader"

    actions = [
      "sqs:ReceiveMessage",
      "sqs:DeleteMessage"
    ]

    resources = [
      aws_sqs_queue.sqs_events.arn
    ]

    principals {
      type = "AWS"
      identifiers = [aws_iam_user.vionlabs.arn]
    }

    effect = "Allow"
  }

  statement {

    sid = "batchespurger"

    actions = [
      "sqs:PurgeQueue"
    ]

    resources = [
      aws_sqs_queue.sqs_batches.arn
    ]

    principals {
      type = "AWS"
      identifiers = [aws_iam_user.vionlabs.arn]
    }

    effect = "Allow"
  }
}


# Combine the policies for SNS into one document
data "aws_iam_policy_document" "combined_sns_policy" {
  statement {

    sid = "eventswriter"

    actions = [
      "sns:Publish"
    ]

    resources = [
      aws_sns_topic.sns_events.arn
    ]

    principals {
      type = "AWS"
      identifiers = [aws_iam_role.Vionlabs_ec2_vm_role.arn]
    }

    effect = "Allow"
  }

  statement {

    sid = "batcheswriter"

    actions = [
      "sns:Publish"
    ]

    resources = [
      aws_sns_topic.sns_batches.arn
    ]

    principals {
      type = "AWS"
      identifiers = [aws_iam_user.vionlabs.arn]
    }

    effect = "Allow"
  }
}


#
# Role for VM
#
resource "aws_iam_role" "Vionlabs_ec2_vm_role" {
    name               = "${var.customer_prefix}-ec2-vm-role"
    path               = "/"
    assume_role_policy = data.aws_iam_policy_document.assume-role.json
    description        = "Allows EC2 instances to call AWS services on your behalf"
    tags               = var.tags
}


#
# Attach policies to the VM Role
#

resource "aws_iam_role_policy" "events-writer-policy" {
    name   = "${var.customer_prefix}-events-writer-policy"
    role   = aws_iam_role.Vionlabs_ec2_vm_role.id
    policy = data.aws_iam_policy_document.events-writer.json
}

resource "aws_iam_role_policy" "batches-reader-policy" {
    name   = "${var.customer_prefix}-batches-reader-policy"
    role   = aws_iam_role.Vionlabs_ec2_vm_role.id
    policy = data.aws_iam_policy_document.batches-reader.json
}

resource "aws_iam_role_policy" "output-bucket-writer-policy" {
    name   = "${var.customer_prefix}-output-bucket-writer-policy"
    role   = aws_iam_role.Vionlabs_ec2_vm_role.id
    policy = data.aws_iam_policy_document.output-bucket-writer.json
}
resource "aws_iam_role_policy" "output-bucket-reader-policy" {
    name   = "${var.customer_prefix}-output-bucket-reader-policy"
    role   = aws_iam_role.Vionlabs_ec2_vm_role.id
    policy = data.aws_iam_policy_document.output-bucket-reader.json
}

resource "aws_iam_role_policy" "secure-bucket-writer-policy" {
    name   = "${var.customer_prefix}-secure-bucket-writer-policy"
    role   = aws_iam_role.Vionlabs_ec2_vm_role.id
    policy = data.aws_iam_policy_document.secure-bucket-writer.json
}

# could be also done as manual operation described in readme
resource "aws_iam_role_policy" "input-bucket-reader-policy" {
    count  = var.input_bucket != "" ? 1 : 0

    name   = "${var.customer_prefix}-input-bucket-reader-policy"
    role   = aws_iam_role.Vionlabs_ec2_vm_role.id
    policy = data.aws_iam_policy_document.input-bucket-reader[0].json
}


#
# Create instance profile to be used by EC2 VM
#

resource "aws_iam_instance_profile" "vm" {
  name  = "${var.customer_prefix}-vm-iam-profile"
  role  = aws_iam_role.Vionlabs_ec2_vm_role.name
}
