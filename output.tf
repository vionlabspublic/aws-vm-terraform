/*
* Unpublished work.
* Copyright 2021-2023 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
# Print out provisioned resources by TF
#

output "aws_region" {
  value       = var.region
}

output "s3_output_bucket_arn" {
  value       = module.remote-infra.s3_output_bucket_arn
}

output "s3_secure_bucket_arn" {
  value       = module.remote-infra.s3_secure_bucket_arn
}

output "sns_batches_arn" {
  value       = module.remote-infra.sns_batches_arn
}

output "sqs_batches_url" {
  value       = module.remote-infra.sqs_batches_url
}

output "sns_events_arn" {
  value       = module.remote-infra.sns_events_arn
}

output "sqs_events_url" {
  value       = module.remote-infra.sqs_events_url
}

output "backend-aws-id" {
  value       = module.remote-infra.backend-aws-id
}

output "backend-aws-secret" {
  value       = module.remote-infra.backend-aws-secret
}

output "vm_script" {
  value       = local.vm_script
}
