/*
* Unpublished work.
* Copyright 2021-2023 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
# Terraform variables used in main script
#


variable "env" {
  description = "Environment - Used for assertion checks"
  type = string
  default = "error"
}

variable "region" {
  description = "Infrastructure: AWS region"
  type        = string
}

variable "tags" {
    type = map
    description = "a set of tags to be used on compatible resources"
    default = {}
}


#
# Infrastructure specific variables
#
# Ref to docs regarding attributes and usage
#
# AWS S3
# https://docs.aws.amazon.com/s3/
#
# AWS SNS
# https://docs.aws.amazon.com/sns/
#
# AWS SQS
# https://docs.aws.amazon.com/sqs/
#
# AWS EC2
# https://docs.aws.amazon.com/ec2/
#

variable "input_bucket" {
  description = "Infrastructure: AWS S3 input bucket name (holds customer assets)"
  type        = string
  default     = ""
}

variable "output_bucket" {
  description = "Infrastructure: AWS S3 output bucket name (holds generated artifacts)"
  type        = string
  default     = "com-vionlabs-bucket-output"
}

variable "secure_bucket" {
  description = "Infrastructure: AWS S3 secure bucket name (holds generated secured artifacts)"
  type        = string
  default     = "com-vionlabs-bucket-secure"
}

variable "batches_sns" {
  description = "Infrastructure: AWS batches SNS name"
  type        = string
  default     = "com-vionlabs-batches-sns"
}

variable "batches_sqs" {
  description = "Infrastructure: AWS batches SQS name"
  type        = string
  default     = "com-vionlabs-batches-sqs"
}

variable "events_sns" {
  description = "Infrastructure: AWS events SNS name"
  type        = string
  default     = "com-vionlabs-events-sns"
}

variable "events_sqs" {
  description = "Infrastructure: AWS events SQS name"
  type        = string
  default     = "com-vionlabs-events-sqs"
}

variable "customer_prefix" {
    type = string
    description = "Prefix to prepend before customer's resource names"
}

variable "eagent_max_load" {
  description = "Infrastructure: Edge Agent number of parallel processings"
  type        = string
  default     = "2"
}
