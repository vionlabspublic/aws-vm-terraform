/*
* Unpublished work.
* Copyright 2021-2023 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


#
# Print out provisioned resources by TF
#

output "vm_instance_public_ip" {
  description = "The public IP of VMs"

  value       = module.aws_vm_vionlabs.vm_instance_public_ip
}

output "vm_instance_private_ip" {
  description = "The private IP of VMs"

  value       = module.aws_vm_vionlabs.vm_instance_private_ip
}
