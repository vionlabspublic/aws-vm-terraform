/*
* Unpublished work.
* Copyright 2021-2024 Vionlabs AB.
*
* VIONLABS AB CONFIDENTIAL
*/


locals {

#
# Construct combined cloud-init + user script
# cloud-init used for early run level usage
# user script is for edge-agent as well as k3s manifests fine-tuning
# this var is to be used by vm instantiation resource
#

  vm_script = <<EOF
Content-Type: multipart/mixed; boundary="===============8752368757152488541=="
MIME-Version: 1.0

--===============8752368757152488541==
Content-Type: text/cloud-config; charset="utf-8"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="cloud-init.yaml"

#cloud-config
cloud_init_modules:
- [scripts-user, always]

--===============8752368757152488541==
Content-Type: text/x-shellscript; charset="utf-8"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="cloud.sh"

#!/bin/bash
# (c) Vionlabs 2021-2024
#
# Create config file for edge-agent
cat > /opt/vionlabs/edge-agent.env <<EOT
EAGENT_MAX_LOAD=${var.eagent_max_load}
EAGENT_CONN_TYPE=aws_sns_sqs
EAGENT_CONN_CONFIG={"client_config":{"region_name":"${var.region}"},"sns_topic_arn":"${module.remote-infra.sns_events_arn}","sqs_queue_url":"${module.remote-infra.sqs_batches_url}","sqs_options":{"endpoint_url":"https://sqs.${var.region}.amazonaws.com/"},"sns_options":{"endpoint_url":"https://sns.${var.region}.amazonaws.com/"}}
EAGENT_DELAY_START_SEC=120
EAGENT_BATCHES_WORK_PERIOD_SEC=60
EAGENT_HEARTBEAT_WORK_PERIOD_SEC=300
EAGENT_HEALTH_WORK_PERIOD_SEC=28800
EAGENT_EXIT_AFTER_SEC=43200
EAGENT_APPLOGLEVEL=WARNING
EOT

# Configure k3s maps which depend on external infra
# The destination dir will be auto-executed by k3s on startup
cat > /var/lib/rancher/k3s/server/manifests/app-env.yaml <<EOT
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: app-env
  namespace: video-ingest
data:
  APPLOGLEVEL: INFO
  ARGOOUTPUTSDIR: /tmp/argo
  ARTIFACTSDIR: "s3://${var.output_bucket}/features"
  EDGE_DEPLOYMENT: "true"
  SECUREARTIFACTSDIR: "s3://${var.secure_bucket}"
EOT

cat > /var/lib/rancher/k3s/server/manifests/cloud-logs-repository.yaml <<EOT
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: cloud-logs-repository
  namespace: video-ingest
data:
  cloud-logs-repo: |
    archiveLogs: true
    s3:
      endpoint: s3.${var.region}.amazonaws.com
      region: ${var.region}
      bucket: ${var.output_bucket}
      keyFormat: logs/{{workflow.name}}/{{pod.name}}
EOT

# Insert customizations (if any) from outside
${var.vm_script}

cat > /var/lib/rancher/k3s/server/manifests/proc-env.yaml <<EOT
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: proc-env
  namespace: video-ingest
data:
  APPLOGLEVEL: INFO
  AWS_REGION: "${var.region}"
  EDGE_DEPLOYMENT: "true"
  S3_ENDPOINT: "s3.${var.region}.amazonaws.com"
  SECUREARTIFACTSDIR: "s3://${var.secure_bucket}"
EOT

--===============8752368757152488541==--
EOF
}
